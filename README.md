# Docker Django

This project can be used as a starting point for any Django project where
developers will collaborate and need a consistent development environment that
is easy to set up.

The intent is that any new developer does not need to install project
dependencies to their machine, but can simply clone this repository and run
either `docker-compose up` or `vagrant up` to have everything running.
Necessary project files are shared between the host/virtual machine/Docker containers.

## Set up

This application can be run using Docker. If you cannot install Docker, a
Vagrantfile is provided for provisioning a Debian virtual machine as well.

First, clone this repository and copy the environment variables template to `.env`.

    $ git clone git@github.com/ryangallen:docker-django.git myproject
    $ cd myproject
    $ cp template.env .env
    $ vi .env  # variables are preset for dev but should be changed in production

### Running with Docker

1. Install [Docker](https://docs.docker.com/install/)
   and [Docker Compose](https://docs.docker.com/compose/install/)
1. Build and run the application containers using docker-compose:

        $ docker-compose up --build -d

1. View the running application in a browser at https://localhost

### Running with Vagrant

1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and
   [Vagrant](https://www.vagrantup.com/downloads.html)
1. Add the [vagrant-docker-compose](https://github.com/leighmcculloch/vagrant-docker-compose)
   Vagrant plug-in and run the development VM:

        $ vagrant plugin install vagrant-docker-compose
        $ vagrant up

1. View the running application in a browser at https://172.16.0.2

### SSL in development

**Note:** Since we are using a self-signed certificate for SSL in development,
your browser will warn that the page connection is insecure. Bypass the warning
by clicking "Advanced" and adding an exception for this certificate.

![Firefox Insecure Connection Warning](https://prod-cdn.sumo.mozilla.net/uploads/gallery/images/2018-07-24-17-48-12-79a9e2.png)

### Helpful Links

- [Docker](https://docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Vagrant](https://www.vagrantup.com/)
- [debian/contrib-stretch64](https://app.vagrantup.com/debian/boxes/contrib-stretch64)
  virtual box (`contrib-*` boxes include the `vboxfs` kernel module for shared folders)
- [Python 3.7](https://www.python.org/)
- [Pipenv](https://pipenv.readthedocs.io/en/latest/)
- [Django](https://djangoproject.com/)
- [Django Rest Framework](https://www.django-rest-framework.org/)
- [PostgreSQL](https://www.postgresql.org/)
- [nginx](https://nginx.org/en/)
